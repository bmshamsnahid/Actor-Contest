let Contest = artifacts.require('./Contest.sol');

contract('Contest', (accounts) => {
	
	it('initializes with two contestants', () => {
		return Contest.deployed().then((instance) => {
			return instance.contestantsCount();
		}).then((count) => {
			assert.equal(count, 2);
		});
	});

	it('it initializes the contestants with the correct value', () => {
		return Contest.deployed().then((instance) => {
			contestInstance = instance;
			return contestInstance.contestants(1);
		}).then((contestant) => {
			assert.equal(contestant[0], 1, 'contains the correct id');
			assert.equal(contestant[1], 'Tom', 'contains the correct name');
			assert.equal(contestant[2], 0, 'contains the current vote count');
			return contestInstance.contestants(2);
		}).then((contestant) => {
			assert.equal(contestant[0], 2, 'contains the correct id');
			assert.equal(contestant[1], 'Jerry', 'contains the correct name');
			assert.equal(contestant[2], 0, 'contains the current vote count');
		});
	});


	it('allows a voter to cast a vote', () => {
		return Contest.deployed().then((instance) => {
			contestInstance = instance;
			contestantId = 1;
			return contestInstance.vote(contestantId, { from: accounts[0] });
		}).then((receipt) => {
			return contestInstance.voters(accounts[0]);
		}).then((voted) => {
			assert(voted, 'this voter was marked as voted');
			return contestInstance.contestants(contestantId);
		}).then((contestant) => {
			var voteCount = contestant[2];
			assert.equal(voteCount, 1, 'increments the contestant vote count');
		});
	});
});