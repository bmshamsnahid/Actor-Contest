pragma solidity 0.4.24;

contract Contest {

	struct Contestant {
		uint id;
		string name;
		uint voteCount;
	}

	mapping(uint => Contestant) public contestants;
	mapping(address => bool) public voters;

	uint public contestantsCount;

	event votedEvent (
		uint indexed _contestantId
	);

	constructor() public {
		addContestant('Tom');
		addContestant('Jerry');
	}

    function addContestant(string _name) private {
    	contestantsCount ++;
    	contestants[contestantsCount] = Contestant(contestantsCount, _name, 0);
    }

    function vote(uint _contestantId) public {
    	require(!voters[msg.sender]);
    	require(_contestantId > 0 && _contestantId <= contestantsCount);
    	contestants[_contestantId].voteCount ++;
    	voters[msg.sender] = true;
    	emit votedEvent(_contestantId);
    } 
}
