# Blockchain Best Actor Contest

#### Casting vote for two popular cartoon charactes, 'Tom' and 'Jerry'


## Prerequisite Technologies In Your Machine
* [Solidity](https://solidity.readthedocs.io/en/v0.4.24/)
* [Node.js](https://nodejs.org/en/)
* [Ethereum](https://www.ethereum.org/)
* [Ganache](https://truffleframework.com/docs/ganache/overview)
* [Truffle](https://truffleframework.com/)
* [MetaMask](https://metamask.io/)

## Installation

```
git clone https://github.com/bmshamsnahid/Actor-Contest  
cd Actor-Contest
npm install
npm run dev
```

NOW BROWSER VIEW AVAILABLE (http://localhost:3000)

## Test

```
truffle test
```
